package com.tribehired.commentsapp.data.model

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Comment(
    var postId: Int,
    var id: Int,
    var name: String,
    var email: String,
    var body: String
) : Parcelable

object CommentDC : DiffUtil.ItemCallback<Comment>() {
    override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Comment, newItem: Comment): Boolean {
        return oldItem.name == newItem.name && oldItem.body == newItem.body &&
                oldItem.email == newItem.email && oldItem.id == newItem.id
    }
}