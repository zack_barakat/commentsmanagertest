package com.tribehired.commentsapp.data.network


import com.tribehired.commentsapp.data.model.Comment
import com.tribehired.commentsapp.data.model.Post
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiService {

    @GET("/posts")
    fun getPosts(): Observable<ArrayList<Post>>

    @GET("/posts/{postId}")
    fun getPostDetail(@Path("postId") postId: Int): Observable<Post>

    @GET("/comments")
    fun getCommentsForPost(@Query("postId") postId: Int): Observable<ArrayList<Comment>>
}