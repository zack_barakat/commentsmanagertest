package com.tribehired.commentsapp.data.repositories

import com.tribehired.commentsapp.data.model.Comment
import com.tribehired.commentsapp.data.model.Post
import com.tribehired.commentsapp.data.network.ApiService
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton


interface PostsRepository {
    fun getPosts(): Observable<ArrayList<Post>>

    fun getPostDetail(postId: Int): Observable<Post>

    fun getCommentsForPost(postId: Int): Observable<ArrayList<Comment>>

    fun getCommentsForPost(postId: Int, query: String): Observable<ArrayList<Comment>>
}

@Singleton
open class PostsRepositoryImpl @Inject constructor(private val apiService: ApiService) :
    PostsRepository {

    private var posts = arrayListOf<Post>()
    private var postCommentsMap = hashMapOf<Int, ArrayList<Comment>>()

    override fun getPosts(): Observable<ArrayList<Post>> {
        return if (posts.isNotEmpty()) {
            Observable.just(posts)
        } else {
            apiService.getPosts()
                .doOnNext {
                    posts = it
                }
        }
    }

    override fun getPostDetail(postId: Int): Observable<Post> {
        val post = posts.firstOrNull { it.id == postId }
        return if (post != null) {
            return Observable.just(post)
        } else {
            apiService.getPostDetail(postId)
        }
    }

    override fun getCommentsForPost(postId: Int): Observable<ArrayList<Comment>> {
        val commentsForPost = postCommentsMap[postId]
        return if (commentsForPost != null) {
            Observable.just(commentsForPost)
        } else {
            apiService.getCommentsForPost(postId)
                .doOnNext { comments ->
                    postCommentsMap[postId] = comments
                }
        }
    }

    override fun getCommentsForPost(postId: Int, query: String): Observable<ArrayList<Comment>> {
        val commentsForPost = postCommentsMap[postId]
        return if (commentsForPost != null) {
            val filteredComments = commentsForPost.filter {
                it.name.contains(query) || it.email.contains(query) || it.body.contains(query)
            }
            Observable.just(ArrayList(filteredComments))
        } else {
            apiService.getCommentsForPost(postId)
                .doOnNext { comments ->
                    postCommentsMap[postId] = comments
                }
                .flatMap { Observable.fromIterable(it) }
                .filter {
                    it.name.contains(query) || it.email.contains(query) || it.body.contains(query)
                }
                .toList()
                .flatMapObservable { Observable.just(ArrayList(it)) }
        }
    }
}