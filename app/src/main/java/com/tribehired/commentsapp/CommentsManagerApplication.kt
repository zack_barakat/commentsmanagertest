package com.tribehired.commentsapp

import com.tribehired.commentsapp.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication


class CommentsManagerApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent
            .builder()
            .application(this)
            .build()
    }

    override fun onCreate() {
        // theme needs to be set before onCreate is called
        super.onCreate()
        instance = this
    }

    companion object {
        lateinit var instance: CommentsManagerApplication
            private set
    }

}
