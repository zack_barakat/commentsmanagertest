package com.tribehired.commentsapp.di.component

import android.app.Application
import android.content.Context
import com.tribehired.commentsapp.CommentsManagerApplication
import com.tribehired.commentsapp.di.modules.*
import com.tribehired.commentsapp.di.qualifiers.ApplicationContext
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        DataModule::class,
        NetworkModule::class,
        ViewModelModule::class,
        ActivityBuildersModule::class,
        FragmentBuildersModule::class
    ]
)
interface AppComponent : AndroidInjector<CommentsManagerApplication> {

    @ApplicationContext
    fun context(): Context

    fun application(): Application

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }
}