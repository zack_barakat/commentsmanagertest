package com.tribehired.commentsapp.di.modules

import com.tribehired.commentsapp.data.repositories.PostsRepository
import com.tribehired.commentsapp.data.repositories.PostsRepositoryImpl
import dagger.Binds
import dagger.Module

@Module(includes = [NetworkModule::class, AppModule::class])
abstract class DataModule {
    @Binds
    abstract fun providePostsRepository(readRepositoryImpl: PostsRepositoryImpl): PostsRepository
}