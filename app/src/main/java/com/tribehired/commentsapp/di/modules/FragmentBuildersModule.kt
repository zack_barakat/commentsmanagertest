package com.tribehired.commentsapp.di.modules

import com.tribehired.commentsapp.ui.postdetail.PostDetailFragment
import com.tribehired.commentsapp.ui.posts.PostsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [ViewModelModule::class])
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributePostsFragment(): PostsFragment

    @ContributesAndroidInjector
    abstract fun contributePostDetailFragment(): PostDetailFragment

}