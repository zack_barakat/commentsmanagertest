package com.tribehired.commentsapp.di.modules

import android.app.Application
import android.content.Context
import com.tribehired.commentsapp.di.qualifiers.ApplicationContext
import dagger.Binds
import dagger.Module

@Module
abstract class AppModule {

    @Binds
    @ApplicationContext
    abstract fun provideApplicationContext(app: Application): Context
}