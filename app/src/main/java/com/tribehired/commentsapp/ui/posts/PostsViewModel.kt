package com.tribehired.commentsapp.ui.posts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tribehired.commentsapp.data.model.Post
import com.tribehired.commentsapp.data.repositories.PostsRepository
import com.tribehired.commentsapp.ui.base.BaseViewModel
import com.tribehired.commentsapp.ui.common.Event
import com.tribehired.commentsapp.ui.common.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PostsViewModel @Inject constructor(
    private val postsRepository: PostsRepository
) : BaseViewModel() {

    private var posts = MutableLiveData<Resource<ArrayList<Post>>>()
    fun posts(): LiveData<Resource<ArrayList<Post>>> = posts

    private var openPost = MutableLiveData<Event<Int>>()
    fun openPost(): LiveData<Event<Int>> = openPost

    init {
        getPosts()
    }

    fun onPostClick(topicId: Int) {
        openPost.postValue(Event(topicId))
    }

    private fun getPosts() {
        val disposable = postsRepository.getPosts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { posts.value = Resource.Loading() }
            .subscribe({ topics ->
                posts.value = Resource.Success(topics)
            }, { e ->
                e.printStackTrace()
                posts.value = Resource.Error(e?.message ?: "Unknown Error")
            })
        addToDisposables(disposable)
    }

}
