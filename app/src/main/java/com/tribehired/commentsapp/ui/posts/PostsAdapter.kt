package com.tribehired.commentsapp.ui.posts

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.tribehired.commentsapp.R
import com.tribehired.commentsapp.data.model.Post
import com.tribehired.commentsapp.data.model.PostDC
import com.tribehired.commentsapp.databinding.PostItemBinding
import com.tribehired.commentsapp.ui.common.DataBoundListAdapter

class PostsAdapter(private val onPostClick: (Int) -> Unit) :
    DataBoundListAdapter<Post, PostItemBinding>(PostDC) {

    override fun createBinding(parent: ViewGroup): PostItemBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.post_item, parent, false
        )
    }

    override fun bind(binding: PostItemBinding, item: Post) {
        binding.post = item
        binding.root.setOnClickListener { onPostClick.invoke(item.id) }
    }
}
