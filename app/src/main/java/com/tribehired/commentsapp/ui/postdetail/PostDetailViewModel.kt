package com.tribehired.commentsapp.ui.postdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tribehired.commentsapp.data.model.Comment
import com.tribehired.commentsapp.data.model.Post
import com.tribehired.commentsapp.data.repositories.PostsRepository
import com.tribehired.commentsapp.ui.base.BaseViewModel
import com.tribehired.commentsapp.ui.common.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PostDetailViewModel @Inject constructor(
    private val postsRepository: PostsRepository
) : BaseViewModel() {

    private var postDetail = MutableLiveData<Resource<Post>>()
    fun postDetail(): LiveData<Resource<Post>> = postDetail

    private var comments = MutableLiveData<Resource<ArrayList<Comment>>>()
    fun comments(): LiveData<Resource<ArrayList<Comment>>> = comments

    private val searchQuerySubject = PublishSubject.create<String>()

    private var postId: Int = 0

    init {
        configureRxSearch()
    }

    fun initViewModelWithParams(postId: Int) {
        this.postId = postId
        getPostDetail(postId)
        getCommentsForPost(postId)
    }

    private fun getPostDetail(postId: Int) {
        val disposable = postsRepository.getPostDetail(postId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { postDetail.value = Resource.Loading() }
            .subscribe({ topics ->
                postDetail.value = Resource.Success(topics)
            }, { e ->
                e.printStackTrace()
                postDetail.value = Resource.Error(e?.message ?: "Unknown Error")
            })
        addToDisposables(disposable)
    }

    private fun getCommentsForPost(postId: Int) {
        val disposable = postsRepository.getCommentsForPost(postId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { postDetail.value = Resource.Loading() }
            .subscribe({ topics ->
                comments.value = Resource.Success(topics)
            }, { e ->
                e.printStackTrace()
                comments.value = Resource.Error(e?.message ?: "Unknown Error")
            })
        addToDisposables(disposable)
    }

    fun onCommentsQueryChange(query: String) {
        searchQuerySubject.onNext(query)
    }

    private fun configureRxSearch() {
        val disposable = searchQuerySubject
            .debounce(300, TimeUnit.MILLISECONDS, Schedulers.io())
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                filterComments(it)
            }, {
                it.printStackTrace()
            })
        addToDisposables(disposable)
    }


    private fun filterComments(query: String) {
        val disposable = postsRepository.getCommentsForPost(postId, query)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { comments.postValue(Resource.Loading()) }
            .subscribe({ friends ->
                comments.postValue(Resource.Success(friends))
            }, { e ->
                e.printStackTrace()
                comments.postValue(Resource.Error(e?.message ?: "Unknown Error"))
            })
        addToDisposables(disposable)
    }
}
