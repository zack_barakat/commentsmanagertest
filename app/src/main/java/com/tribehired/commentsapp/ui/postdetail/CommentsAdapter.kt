package com.tribehired.commentsapp.ui.postdetail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.tribehired.commentsapp.R
import com.tribehired.commentsapp.data.model.Comment
import com.tribehired.commentsapp.data.model.CommentDC
import com.tribehired.commentsapp.databinding.CommentItemBinding
import com.tribehired.commentsapp.databinding.PostItemBinding
import com.tribehired.commentsapp.ui.common.DataBoundListAdapter

class CommentsAdapter : DataBoundListAdapter<Comment, CommentItemBinding>(CommentDC) {

    override fun createBinding(parent: ViewGroup): CommentItemBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.comment_item, parent, false
        )
    }

    override fun bind(binding: CommentItemBinding, item: Comment) {
        binding.comment = item
    }
}
