package com.tribehired.commentsapp.ui.postdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribehired.commentsapp.R
import com.tribehired.commentsapp.di.ViewModelProviderFactory
import com.tribehired.commentsapp.ui.common.Resource
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.post_detail_fragment.*
import javax.inject.Inject

class PostDetailFragment : DaggerFragment(), SearchView.OnQueryTextListener {

    companion object {
        const val ARG_POST_ID = "post_id"
        fun newInstance(postId: Int): PostDetailFragment {
            val bundle = Bundle().apply { putInt(ARG_POST_ID, postId) }
            return PostDetailFragment().apply { arguments = bundle }
        }
    }

    private lateinit var adapter: CommentsAdapter
    @Inject
    lateinit var viewModelFactory: ViewModelProviderFactory
    private lateinit var viewModel: PostDetailViewModel

//    internal var listener: PostsListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.post_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(PostDetailViewModel::class.java)
        viewModel.initViewModelWithParams(arguments?.getInt(ARG_POST_ID) as Int)
        setupView()
        bindViewModel()
    }

    private fun setupView() {
        svComments.setOnQueryTextListener(this)
        (activity as? AppCompatActivity)?.supportActionBar?.title = "Post Detail"
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        adapter = CommentsAdapter()
        val layoutManager = LinearLayoutManager(activity)
        rvComments.layoutManager = layoutManager
        rvComments.adapter = adapter
    }

    private fun bindViewModel() {
        viewModel.postDetail().observe(viewLifecycleOwner, Observer { post ->
            when (post) {
                is Resource.Loading -> progressBar.visibility = View.VISIBLE
                is Resource.Success -> {
                    progressBar.visibility = View.GONE
                    tvTitle.text = post.data?.title
                    tvBody.text = post.data?.body
                }
                is Resource.Error -> progressBar.visibility = View.GONE
            }
        })

        viewModel.comments().observe(viewLifecycleOwner, Observer { comments ->
            when (comments) {
                is Resource.Loading -> progressBar.visibility = View.VISIBLE
                is Resource.Success -> {
                    progressBar.visibility = View.GONE
                    adapter.submitList(comments.data)
                }
                is Resource.Error -> progressBar.visibility = View.GONE
            }
        })
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        svComments.clearFocus()
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        viewModel.onCommentsQueryChange(newText ?: "")
        return true
    }
}
