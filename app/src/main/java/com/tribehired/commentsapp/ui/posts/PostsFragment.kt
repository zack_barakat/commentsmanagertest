package com.tribehired.commentsapp.ui.posts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribehired.commentsapp.R
import com.tribehired.commentsapp.di.ViewModelProviderFactory
import com.tribehired.commentsapp.ui.common.Resource
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.posts_fragment.*
import javax.inject.Inject

class PostsFragment : DaggerFragment() {

    companion object {
        fun newInstance() = PostsFragment()
    }

    private lateinit var adapter: PostsAdapter
    @Inject
    lateinit var viewModelFactory: ViewModelProviderFactory
    private lateinit var viewModel: PostsViewModel

    private var listener: PostsListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.posts_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(PostsViewModel::class.java)
        setupView()
        bindViewModel()
    }

    private fun setupView() {
        (activity as? AppCompatActivity)?.supportActionBar?.title = "Posts"
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        adapter = PostsAdapter { viewModel.onPostClick(it) }
        val layoutManager = LinearLayoutManager(activity)
        rvPosts.layoutManager = layoutManager
        rvPosts.adapter = adapter
    }

    private fun bindViewModel() {
        viewModel.posts().observe(viewLifecycleOwner, Observer { posts ->
            when (posts) {
                is Resource.Loading -> progressBar.visibility = View.VISIBLE
                is Resource.Success -> {
                    progressBar.visibility = View.GONE
                    adapter.submitList(posts.data)
                }
                is Resource.Error -> progressBar.visibility = View.GONE
            }
        })
        viewModel.openPost().observe(viewLifecycleOwner, Observer { event ->
            event.getContentIfNotHandled()?.let { listener?.openPost(it) }
        })
    }

    fun setPostsListener(listener: PostsListener) {
        this.listener = listener
    }

    interface PostsListener {
        fun openPost(postId: Int)
    }
}
