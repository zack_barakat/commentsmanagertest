# Comments Manager Android App

## 1 App Architecture
### 1.1 General App Architecture
The application uses clean architecture principles partically and contains two layer presentation layer, and data layer.


**Important Note:** All viewmodels and classes interacts with the `repositories` **interfaces** only and does not interact with api service directly.
#### 1.1 Data Layer:
Repository pattren has been implemented as data source of truth for the viewmodels it can be found under [`data.repositories`] package. 
Th repository have remote api service object a remote data source, with this pattren offline support can be easily implemented.
Dagger2 should maintain **only one copy** of each repository per app session (Singlton behaviour).

#### 1.2 UI architecture (Persentation Layer):
Model, View, ViewModel known as MVVM is the the architecture pattern used to develop the Comments Manager app persentatio layer.
**Model:** It is responsible for handling the data part of the application.
**View:** It is responsible for laying out the views with specific data on the screen ex: Fragment, Activity, View.
**ViewModel:** an object which describes the behavior of View logic depending on the result of Model work. You can call it a behavior model of View. It can be a rich text formatting as well as a component visibility control logic or condition display, such as loading, error, blank screens, etc.

To read more about MVVM Architecture you may refer to these links:
- [MVVM Architecture and architecture components](https://developer.android.com/topic/libraries/architecture)
- [MVVM Architecture with Live Data](https://proandroiddev.com/mvvm-architecture-viewmodel-and-livedata-part-1-604f50cda1)

## 2 Main Libraries used

* [Retrofit](http://square.github.io/retrofit/) - REST client library for android
* [Dagger2](https://google.github.io/dagger/android.html) - Dependency injection framework
* [RxJava](https://github.com/ReactiveX/RxJava) and [RxAndroid](https://github.com/ReactiveX/RxAndroid) - Reactive programming, simplifies work with threading and concurrency in java and android.
* [DataBinding](https://developer.android.com/topic/libraries/data-binding) - Allows you to bind UI components in your layouts to data sources in your app using a declarative format


